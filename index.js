console.log ("Hello World!");

let trainer ={
	name: "Ash Ketchum",
	age: 10,
	friends: {
			hoenn: ["May", "Max"],
			kanto: ["Brock", "Misty"]
			},
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Balbasaur"],
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
	
}
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method");
trainer.talk();

function Pokemon(name, level, health, attack, tackle){
	
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){
		let remainingHealth = (Number(target.health - this.attack))
		target.health = remainingHealth;
		
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to " + Number(target.health - this.attack))

				if (remainingHealth <= 0 ){
					target.faint()
		}

	}
	this.faint = function(){
		console.log(this.name + " fainted.")
	}
	this.displayPokemon = function(){
		console.log(target);
	}
}

let pikachu = new Pokemon("Pikachu", 12, 24, 12);
let geodude = new Pokemon("Geodude", 8, 16, 8, pikachu);
let mewtwo = new Pokemon("Mewtwo", 100, 200, 100, geodude);



console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);


console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);
